## Scott Buckley

This repo (hosted on bitbucket.io) has moved to github, accessible at [http://scottbuckley.github.io](http://scottbuckley.github.io).

Why did I move this repo? Because bitbucket does not allow for CNAME redirects, and I wanted to use a custom domain to redirect to this repo. Github does support this.